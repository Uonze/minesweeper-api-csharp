namespace MinesweeperUnitTests
{
    using System;
    using System.Linq;
    using Minesweeper.Domain.Model;
    using Xunit;

    public class MinesweeperUnitTests
    {
        [Fact]
        public void Minesweeper_ParameterlessConstructorIsCalled_MinesweeperWith25CellIsCreated()
        {
            // Arrange
            const int ExpectedCellCount = 25;

            // Act
            var result = new Minesweeper();

            // Arrange
            Assert.Equal(ExpectedCellCount, result.Cells.Count());
        }

        [Fact]
        public void Minesweeper_ParameterlessConstructorIsCalled_MinesweeperWithIdNotNullAndNotEmptyIsCreated()
        {
            // Act
            var result = new Minesweeper();

            // Assert
            Assert.NotEqual(Guid.Empty, result.Id);
        }
    }
}