﻿namespace Minesweeper.Application.Dto
{
    using System;

    public class Cell
    {
        public Guid Id { get; set; }

        public int PosX { get; set; }

        public int PosY { get; set; }
    }
}