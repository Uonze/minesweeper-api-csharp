namespace Minesweeper.Domain.Model
{
    using System;

    public class Cell
    {
        public Guid Id { get; private set; }

        public int PosX { get; private set; }

        public int PosY { get; private set; }

        public Cell()
        {
            this.Id = Guid.NewGuid();
        }

        public Cell(int posX, int posY) : this()
        {
            this.PosX = posX;
            this.PosY = posY;
        }
    }
}