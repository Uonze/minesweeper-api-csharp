﻿namespace Minesweeper.Domain.Model
{
    using System;
    using System.Collections.Generic;

    public class Minesweeper
    {
        private const int MatrixSize = 5;

        public Guid Id { get; private set; }

        public int Score { get; private set; }

        public IList<Cell> Cells { get; private set; }

        public Minesweeper()
        {
            this.Id = Guid.NewGuid();

            this.Cells = new List<Cell>();

            for (var i = 0; i < MatrixSize; i++)
            {
                for (int j = 0; j < MatrixSize; j++)
                {
                    this.Cells.Add(new Cell(i, j));
                }
            }
        }
    }
}