﻿namespace Minesweeper.Application.Services
{
    using System.Threading.Tasks;
    using Minesweeper.Application.Dto;

    public interface IMinesweeperService
    {
        Task<Minesweeper> CreateNewGame();
    }
}