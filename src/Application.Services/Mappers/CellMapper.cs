﻿namespace Minesweeper.Application.Services.Mappers
{
    using Dto = Minesweeper.Application.Dto;
    using Model = Minesweeper.Domain.Model;

    public static class CellMapper
    {
        public static Dto.Cell ToDto(this Model.Cell cell)
        {
            return new Dto.Cell
            {
                Id = cell.Id,
                PosX = cell.PosX,
                PosY = cell.PosY
            };
        }
    }
}