﻿namespace Minesweeper.Application.Services.Mappers
{
    using System.Collections.Generic;
    using System.Linq;
    using Dto = Minesweeper.Application.Dto;
    using Model = Minesweeper.Domain.Model;

    public static class MinesweeperMapper
    {
        public static IEnumerable<Dto.Minesweeper> ToDto(this IEnumerable<Model.Minesweeper> minesweepers)
        {
            return minesweepers.Select(m => m.ToDto());
        }

        public static Dto.Minesweeper ToDto(this Model.Minesweeper minesweeper)
        {
            return new Dto.Minesweeper
            {
                Id = minesweeper.Id,
                Score = minesweeper.Score,
                Cells = minesweeper.Cells.Select(c => c.ToDto())
            };
        }
    }
}