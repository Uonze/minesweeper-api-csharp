FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 1248
EXPOSE 44376

FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /src
COPY src/Presentation.WebApi/Presentation.WebApi.csproj Presentation.WebApi/
COPY src/Application.Dto/Application.Dto.csproj Application.Dto/
COPY src/Application.Services/Application.Services.csproj Application.Services/
COPY src/Domain.Model/Domain.Model.csproj Domain.Model/
RUN dotnet restore Presentation.WebApi/Presentation.WebApi.csproj
COPY src/ .
WORKDIR /src/Presentation.WebApi
RUN dotnet build Presentation.WebApi.csproj -c Release -o /app

FROM build AS publish
RUN dotnet publish Presentation.WebApi.csproj -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Presentation.WebApi.dll"]
